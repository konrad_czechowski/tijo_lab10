package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.moves.PieceMoves;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private final PieceMoves pieceMoves;

    @Autowired
    public ChessServiceImpl(PieceMoves pieceMoves) {
        this.pieceMoves = pieceMoves;
    }

    @Override
    public Boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        return pieceMoves.isCorrect(figureMoveDto);
    }
}
