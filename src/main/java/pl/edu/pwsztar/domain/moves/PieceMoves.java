package pl.edu.pwsztar.domain.moves;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.mapper.PiecePositionMapper;
import pl.edu.pwsztar.domain.model.ChessPiecePosition;


@Component
public class PieceMoves {

    private final PiecePositionMapper piecePositionMapper;

    @Autowired
    public PieceMoves(PiecePositionMapper piecePositionMapper) {
        this.piecePositionMapper = piecePositionMapper;
    }

    public Boolean isCorrect(FigureMoveDto figureMoveDto) {
        ChessPiecePosition start = piecePositionMapper.convert(figureMoveDto.getStart());
        ChessPiecePosition dest = piecePositionMapper.convert(figureMoveDto.getDestination());

        Boolean moveIsCorrect = false;

        switch (figureMoveDto.getType()) {
            case KING:
            case PAWN:
            case KNIGHT:
            case ROCK:
            case QUEEN:
                break;
            case BISHOP:
                moveIsCorrect = bishopMove(start, dest);
                break;
        }
        return moveIsCorrect;
    }

    private Boolean bishopMove(ChessPiecePosition start, ChessPiecePosition dest) {
        return Math.abs(start.getX() - dest.getX()) == Math.abs(start.getY() - dest.getY());
    }
}
