package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.model.ChessPiecePosition;

@Component
public class PiecePositionMapper implements Converter<String, ChessPiecePosition> {

    @Override
    public ChessPiecePosition convert(String from) {
        String[] position = from.split("_");
        Integer x = position[0].charAt(0)-96;
        Integer y = position[1].charAt(0)-96;
        return new ChessPiecePosition(x, y);
    }
}
