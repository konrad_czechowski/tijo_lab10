package pl.edu.pwsztar.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ChessPiecePosition {

    private final Integer x;
    private final Integer y;
}
